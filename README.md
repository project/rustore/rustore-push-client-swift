# RSPushClient
SDK для кастомизации Push-уведомлений

## Возможности
- Скрытие дупликатов уведомлений
- Отображение изображения в правой части уведомления
- Отображение и кеширование изображения в левой части уведомления ("аватарка")
- Скачивание изображения для расхлопа уведомления
- Отправка аналитики в chekhov.dev.rustore.devmail.ru

## Установка

### SPM

1. Выберите ваш проект в Xcode
2. Добавьте SPM зависимость в Xcode, указав адрес [https://gitflic.ru/project/rustore/rustore-push-client-swift.git](https://gitflic.ru/project/rustore/rustore-push-client-swift.git)

Для подключения в `Package.swift` укажите название пакета `rustore-push-client-swift` и имя продукта `RSPushClient`:
```swift
let package = Package(
    dependencies: [
        .package(
            url: "https://gitflic.ru/project/rustore/rustore-push-client-swift.git", 
            from: "1.0.0"
        )
    ],
    targets: [
        .target(
            name: "MyTarget",
            dependencies: [
                .product(name: "RSPushClient", package: "rustore-push-client-swift")
            ]
        )
    ]
)
```

### CocoaPods

Podfile:
```ruby
pod 'PSPushClient', '~> 1.0.0'
```

## Интеграция в приложение
### Основной target

```swift
class AppDelegate: UIApplicationDelegate, UNUserNotificationCenterDelegate, ActionHandling {
    func application(
        _ application: UIApplication, 
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        PushClient.configure(
            appGroupID: <some_app_group_id>,
            actionHandler: self,
            externalAnalyticsTracker: someExternalAnalytics, // если нужно дополнительно логировать уведомления, иначе nil
            cacheParameters: .default // если значения по-умолчанию подходят, то nil
        )
        
        UNUserNotificationCenter.current().delegate = self
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        // Если уведомление не относится к библиотеке, то вернется переданный completionHandler
        let completion = PushClient.userNotificationCenter(
            center,
            willPresent: notification,
            withCompletionHandler: completionHandler
        )
        completion([.list, .sound, .banner])
    }
  
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        // Если уведомление не относится к библиотеке, то вернется переданный completionHandler
        let completion = PushClient.didReceive(response: response, withCompletionHandler: completionHandler)
        completion()
    }
    
    // Очищаем кеш в каком-либо пользовательском сценарии, например, при разлогине
    func logOut() {
        PushClient.clearCache()
    }
    
    func open(url: URL) {
        // Обработать открытие ссылки
    }
}
```

### NotificationServiceExtension

```swift
class NotificationService: UNNotificationServiceExtension {
    override init() {
        PushServiceExtensionClient.configure(
            appGroupId: <some_app_group_id>,
            externalAnalyticsTracker: someExternalAnalytics // если нужно дополнительно логировать уведомления, иначе nil
        )
        super.init()
    }

    override func didReceive(
        _ request: UNNotificationRequest, 
        withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void
    ) {
        PushServiceExtensionClient.didRecieveExtensionNotification(request, contentHandler: contentHandler)
    }

    override func serviceExtensionTimeWillExpire() {
        PushServiceExtensionClient.serviceExtensionTimeWillExpire()
    }
}
```

### NotificationContentExtension

```swift
class PushNotificationViewController: UIViewController, UNNotificationContentExtension {
    private let contentImageView = UIImageView()
    // ...
    // Настройка контроллера
    // ...
    func didReceive(_ notification: UNNotification) {
        PushContentExtensionService.didReceive(notification) { [weak self] image in
            self?.contentImageView.image = image
        }
  }
}
```

## UI-тестирование Push-уведомлений

В рамках UI-тестирования можно проверить только работу основного таргета (NotificationServiceExtension и NotificationContentExtension не вызываются при локальной отправке уведомлений (не через APNS)). Поэтому для тестирования можно использовать отправку локального уведомления:

```swift
func testPushNotification() {
    // Отправить локальное уведомление
    RSPushClient.PushTestService.sendPush(
      showWhenActive: true,
      isDuplicate: false,
      url: URL(string: stringUrl),
      extraInfo: ["key": "value"]
    )
}
```
