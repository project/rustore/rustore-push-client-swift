// swift-tools-version:5.6

import PackageDescription

let package = Package(
    name: "RSPushClient",
    platforms: [
        .iOS(.v15)
    ],
    products: [
        .library(
            name: "RSPushClient",
            targets: ["RSPushClient"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "RSPushClient",
            url: "https://artifactory-external.vkpartner.ru/artifactory/rustore-swift/push-client/1.0.0/RSPushClient.xcframework.zip", 
            checksum: "dd723f91299e2bbaacb57a3b786c61e02ec7f421073892fa68cde06e0f0a6b63"
        )
    ],
    swiftLanguageVersions: [.v5]
)
